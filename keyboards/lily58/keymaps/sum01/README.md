Why? Because most of the existing keymaps don't use hjkl positioning for arrows, have mouse movement, or laptop/media keys.  
This does all three of those.

Shift and ctrl are also swapped (versus default) to be more "normal".  
Backtick/tilde is moved down to right-shift's old spot for better ergonomics.  
Equals/plus is put on one key in the RAISE layer by using shift+raise for plus.  
The same is done for backslash/pipe.

This **doesn't** support OLED or LED's because I have neither on mine.
